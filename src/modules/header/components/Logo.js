import React from "react";
import LogoIcon from "assets/img/logo.png";

const Logo = () => {
  return <img src={LogoIcon} alt="Landr Logo" />;
};

export default Logo;
