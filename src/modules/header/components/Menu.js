import React from "react";
import style from "./Menu.module.scss";

const Menu = () => {
  return (
    <ul className={style["menu"]}>
      <li className={style["menu__item"]}>Home</li>
      <li className={style["menu__item"]}>Features</li>
      <li className={style["menu__item"]}>Info</li>
      <li className={style["menu__item"]}>Sreenshots</li>
      <li className={style["menu__item"]}>Blog</li>
      <li className={style["menu__item"]}>PressKit</li>
      <li className={`${style["menu__item"]} ${style["menu__item--download"]}`}>
        Download
      </li>
    </ul>
  );
};

export default Menu;
