import React from "react";
import Logo from "./Logo";
import Menu from "./Menu";
import style from "./TopHeader.module.scss";

const TopHeader = () => {
  return (
    <div className={style["top-header"]}>
      <Logo />
      <Menu />
    </div>
  );
};

export default TopHeader;
