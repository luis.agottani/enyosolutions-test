import React from "react";
import style from "./Slider.module.scss";
import Mockup from "assets/img/mockup.png";

const SliderHeader = () => {
  return (
    <div className={style["slider"]}>
      <h1 className={style["slider__title"]}>
        Lorem <span className={style["slider__title--green"]}>ipsum dolor</span>{" "}
        sit amet, est te minim consectetuer usu sale volutpa
      </h1>
      <div className={style["slider__mockup"]}>
        <img src={Mockup} alt="" />
      </div>
    </div>
  );
};

export default SliderHeader;
