import React from "react";
import TopHeader from "./components/TopHeader";
import style from "./Header.module.scss";
import SliderHeader from "./components/Slider";

const Header = () => {
  return (
    <header className={style["header"]}>
      <div className={style["header__content"]}>
        <TopHeader />
        <SliderHeader />
      </div>
    </header>
  );
};

export default Header;
