import React from "react";
import Plataform from "./components/Plataform";

const AppDownload = () => {
  return (
    <div className="appDownload">
      <div className="appDownload__content">
        <h2 className="appDownload__title">
          Download this <span class="appDownload__title--green">awesome</span>{" "}
          app
        </h2>
        <p className="appDownload__sub">
          Available on all modern operating systems
        </p>
        <ul className="appDownload__plataforms">
          <Plataform type="google" />
          <Plataform type="mac" />
          <Plataform type="ios" />
        </ul>
      </div>
    </div>
  );
};

export default AppDownload;
