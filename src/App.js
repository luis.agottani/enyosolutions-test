import React from "react";
import "./App.css";

import MainPage from "./modules/MainPage";

function App() {
  return <MainPage />;
}

export default App;
